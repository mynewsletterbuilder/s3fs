# s3fs-fuse : mount an Amazon S3 bucket as a block device #

A Puppet module to install & configure S3 buckets

### License ###

Copyright (C) 2016 Ada Mancini ada@jbanetwork.com

Derived from https://github.com/s3fs-fuse/s3fs-fuse (C) 2010 Randy Rizun rrizun@gmail.com

Licensed under the GNU GPL version 2