define s3fs::config (
$bucket,
$mountpoint,
$options = 'allow_other',
) {
  if !defined( File[$mountpoint] ) {
    mount { $mountpoint:
      ensure   => mounted,
      device   => "s3fs#${bucket}",
      name     => $mountpoint,
      fstype   => 'fuse',
      options  => $options,
      remounts => false,
      require  => [ Class['s3fs'], File[$mountpoint] ],
    }
  }
}