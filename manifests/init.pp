class s3fs (
$repo_dir = '/usr/src/s3fs-fuse',
$git_source = 'https://github.com/s3fs-fuse/s3fs-fuse.git',
) {

  # get dependency packages to build s3fs-fuse
  $packages = [
    'automake',
    'autotools-dev',
    'g++',
    'libcurl4-openssl-dev',
    'mime-support',
    'libcurl4-gnutls-dev',
    'libfuse-dev',
    'libssl-dev',
    'libxml2-dev',
    'libtool',
    'make',
    'pkg-config',
  ]

  package { $packages:
    ensure => latest,
  }

  vcsrepo { $repo_dir:
    ensure   => present,
    provider => git,
    source   => $git_source,
  }

  # configure & install s3fs-fuse
  exec { 'autogen-s3fs':
    cwd      => $repo_dir,
    provider => 'shell',
    command  => './autogen.sh',
    creates  => "${repo_dir}/configure",
    require  => Vcsrepo[$repo_dir],
  }

  exec { 'configure-s3fs':
    cwd      => $repo_dir,
    provider => 'shell',
    command  => './configure --prefix=/usr --with-openssl',
    creates  => "${repo_dir}/Makefile",
    require  => Exec['autogen-s3fs'],
  }

  exec { 'install-s3fs':
    cwd      => $repo_dir,
    provider => 'shell',
    command  => 'make && make install',
    creates  => '/usr/bin/s3fs',
    require  => Exec['configure-s3fs'],
  }
}