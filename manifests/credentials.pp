class s3fs::credentials (
$access_key,
$secret_access_key,
) {

  validate_string($access_key)
  validate_string($secret_access_key)
  
  file { '/root/s3fs-passwd':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => template('s3fs/s3fs-passwd.erb'),
  }
}